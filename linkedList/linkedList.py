# Node of a Singly Linked List
class Node:
    # constructor
    def __init__(self, data):
        self.data = data
        self.next = None
         # method for setting the data field of the node
    def setData(self, data):
        self.data = data
    # method for getting the data field of the node
    def getData(self):
        return self.data
      # method for setting the next field of the node
    def setNext(self, next):
        self.next = next
       # method for getting the next field of the node
    def getNext(self):
        return self.next
    # returns true if the node points to another node
    def hasNext(self):
            return self.next != None
    def __str__(self):
        return str(self.data)

# class for defining a linked list
class LinkedList(object):
    # initializing a list
    def __init__(self):
        self.length = 0
        self.head = None
    # method to add a node in the linked list
    def addNode(self, node):
        if self.length == 0:
            self.addBeg(node)
        else:
            self.addLast(node)
    # method to add a node at the beginning of the list with a data
    def addBeg(self, node):
        newNode = node
        newNode.next = self.head
        self.head = newNode
        self.length += 1
    # method to add a node after the node having the data=data. The data of the new node is value2
    def addAfterValue(self, data, node):
        tempNode = self.head
        count = 0
        while tempNode != None:
            if tempNode.getData() == data:
                newNode = node
                newNode.next = tempNode.next
                tempNode.next = newNode
                self.length += 1
                return
            else:
                tempNode = tempNode.next
        print ("Data passed in was not found in the linked list")
   # method to add a node at a particular position
    def addAtPos(self, pos, node):
        if (pos > self.length):
            print("Position given surpasses the length of the Node")
            return
#    # method to add a node at the end of a list
    def addLast(self, node):
        tempNode = self.head
        while tempNode.next != None:
            tempNode = tempNode.next
        newNode = node
        newNode.next = None
        tempNode.next = newNode
        self.length += 1
    # method to delete the first node of the linked list
    def deleteBeg(self):
        if self.length != 0:
            self.head = self.head.next
            self.length -= 1
        else:
            print("List len is 0, nothing to remove")

    def deleteLast(self):
        if self.length == 0:
            print("Nothing to remove. List len is 0")
        else:
            tempNode = self.head
            while tempNode.next != None:
                tempNode = tempNode.next
            tempNode.next = None
#    # method to delete a node at a particular position
    def deleteAtPos(self, pos):
        if self.length <= pos:
            print("Pos is out of bounds")
        else:
            count = 0
            currentNode =  self.head
            while (count < pos):
                tempNode = currentNode
                currentNode =  currentNode.next
                count += 1
            tempNode.next = currentNode.next
            self.length -= 1
#    # returns the length of the list
    def getLength(self):
        return self.length
#    # returns the first element of the list
    def getFirst(self):
        return self.head
#    # returns the last element of the list
    def getLast(self):
        if self.length == 0:
            print("List is empty")
            return
        tempNode = self.head
        while tempNode.next != None:
            tempNode = tempNode.next
        return tempNode
#    # returns node at any position
    def getAtPos(self, pos):
        if self.length >= pos:
            tempNode = self.head
            count = 0
            while (count < pos):
                tempNode = tempNode.next
                count += 1
            return tempNode
        else:
            print("Pos given is out of bounds")
            return None

    # method to check for cycles
    def checkCycle(self):
        if self.head == None or self.head.next == None:
            return False
        slow = self.head
        fast = self.head.next
        while (slow is not fast):
            slow = slow.next
            try:
                fast = fast.next.next
            except AttributeError:
                return False
        print("There is a cycle")
        return True

    def findCycleBeg(self):
        if self.head == None or self.head.next == None:
            return False
        slow = self.head.next
        fast = slow.next
        while (slow is not fast):
            slow = slow.next
            try:
                fast = fast.next.next
            except AttributeError:
                print("No Cycle")
                return None
        print("slow and fast met at: %s" % slow)
        slow = self.head
        while slow is not fast:
            slow = slow.next
            fast = fast.next
        return slow
    # method to reverse the linked list
    def reverseList(self, node):
        if node is not None:
            right = node.getNext()
            if self.head is not node:
                node.setNext(self.head)
                self.head = node
            else:
                node.setNext(None)
            self.reverseList( right )
    # Display the linked list in reverse
    def reverseDisplay(self, node):
        if node != None:
            if node.getNext() != None:
                self.reverseDisplay(node.getNext())
            print(node)
    # method to print the whole list
    def __str__(self):
        listPrint = "["
        temp = self.head
        while temp.next != None:
            listPrint += "%d," % temp.data
            temp = temp.next
        listPrint += "%d]" % temp.data
        return listPrint

node1 = Node(1)
node2 = Node(2)
node3 = Node(3)
node4 = Node(4)
node5 = Node(5)
node6 = Node(6)
node7 = Node(7)
node8 = Node(8)
node9 = Node(9)
node10 = Node(10)

ll = LinkedList()
ll.addNode(node1)
ll.addNode(node2)
ll.addNode(node3)
ll.addNode(node4)
ll.addNode(node5)
print(ll)
print("Test addAfterValue")
ll.addAfterValue(8,Node(5))
print(ll)

print("Test deleteBeg")
ll.deleteBeg()
print(ll)

print("Test deleteAtPos")
ll.deleteAtPos(2)
print(ll)

print("Test getLast")
print("%d" % ll.getLast().data)
print(ll)
print("Test getAtPosition")
print("Len: %d, Pos 2: %d" % (ll.length, ll.getAtPos(2).data))

print("Check if Cycle exists")
ll2 = LinkedList()
ll2.addNode(node1)
print(ll2)
print(ll2.checkCycle())
ll2.addNode(node2)
ll2.addNode(node3)
ll2.addNode(node4)
ll2.addNode(node5)
ll2.addNode(node6)
ll2.addNode(node7)
ll2.addNode(node8)
#print(ll2)
#print(ll2.getAtPos(7))
#ll2.getAtPos(7).setNext(ll2.head.next.next.next)
#print("added cycle at pos 7 to pos 3. Is there Cycle?")
#print(ll2.checkCycle())
#print("Where does it begin?")
#print(ll2.findCycleBeg())
print("Test reverse method linked list")
print("Before: ", ll2)
ll2.reverseList(ll2.head)
print("After: ", ll2)

print("Display list in reverse: ")
ll2.reverseDisplay(ll2.head)
